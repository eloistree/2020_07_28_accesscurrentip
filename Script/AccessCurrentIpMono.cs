﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class AccessCurrentIpMono : MonoBehaviour
{

    public List<string> ips;
    public bool ip4=true;
    public bool ip6;
    public IpsChangeEvent m_displayDebug;
    public void Start()
    {
        Refresh();
    }

    [System.Serializable]
    public class IpsChangeEvent : UnityEvent<string> { }

    public Regex m_ipv4 = new Regex("\\d*\\.\\d*\\.\\d*\\.\\d*");
    public void Refresh() {
        ips.Clear();
        string strHostName = Dns.GetHostName();
        IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);
        foreach (IPAddress ipaddress in iphostentry.AddressList)
        {
            string ip = ipaddress.ToString();
            if (ip4 && m_ipv4.IsMatch(ip))
                ips.Add(ip); 
            if (ip6 && !m_ipv4.IsMatch(ip))
                ips.Add(ip);
        }
        m_displayDebug.Invoke(string.Join(",", ips));
    }
    public void Test()
    {

        //foreach (NetworkInterface netInterface in NetworkInterface.GetAllNetworkInterfaces())
        //{
        //    Console.WriteLine("Name: " + netInterface.Name);
        //    Console.WriteLine("Description: " + netInterface.Description);
        //    Console.WriteLine("Addresses: ");
        //    IPInterfaceProperties ipProps = netInterface.GetIPProperties();
        //    foreach (UnicastIPAddressInformation addr in ipProps.UnicastAddresses)
        //    {
        //        Console.WriteLine(" " + addr.Address.ToString());
        //    }
        //    Console.WriteLine("");
        //}
    }
    private void OnValidate()
    {
        Refresh();
    }
}


